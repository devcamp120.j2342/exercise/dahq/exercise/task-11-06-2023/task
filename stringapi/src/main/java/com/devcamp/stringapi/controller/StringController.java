package com.devcamp.stringapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.stringapi.service.StringService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class StringController {
    @Autowired
    private StringService stringService;

    @GetMapping("/task1/{string}")
    public String Task1(@PathVariable String string) {
        String re = stringService.daoNguocChuoi(string);
        return re;
    }

    @GetMapping("/task2/{string}")
    public boolean Task2(@PathVariable String string) {
        boolean re = stringService.chuoiPalindrom(string);
        return re;
    }

    @GetMapping("/task3/{string}")
    public String Task3(@PathVariable String string) {
        String result = stringService.Task3(string);
        return result;
    }

    @GetMapping("/task4/{str1}/{str2}")
    public String Task4(@PathVariable String str1, @PathVariable String str2) {
        String res = stringService.Task4(str1, str2);
        return res;
    }

}
