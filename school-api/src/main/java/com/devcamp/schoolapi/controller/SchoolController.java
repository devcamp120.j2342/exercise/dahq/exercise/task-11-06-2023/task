package com.devcamp.schoolapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.schoolapi.model.School;
import com.devcamp.schoolapi.service.SchoolService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<School> schoolsList() {
        ArrayList<School> schools = schoolService.allSchools();
        return schools;
    }

    @GetMapping("/school-info-id/{index}")
    public School schoolId(@PathVariable int index) {
        School school = schoolService.schoolById(index);
        return school;
    }

    @GetMapping("/school-nonumber/{noNumber}")
    public ArrayList<School> schoolsListNoNumber(@PathVariable int noNumber) {
        ArrayList<School> schools = schoolService.schoolByNoNumber(noNumber);
        return schools;
    }

}
