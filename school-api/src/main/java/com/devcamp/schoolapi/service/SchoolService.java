package com.devcamp.schoolapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.schoolapi.model.School;

@Service
public class SchoolService {
    @Autowired
    private ClassroomService classroomService;
    School school1 = new School(1, "SchoolA", "VN");
    School school2 = new School(2, "SchoolB", "VN");
    School school3 = new School(3, "SchoolC", "VN");

    public ArrayList<School> allSchools() {
        school1.setClassrooms(classroomService.classroomsA());
        school2.setClassrooms(classroomService.classroomsB());
        school3.setClassrooms(classroomService.classroomsC());
        ArrayList<School> schools = new ArrayList<>();
        schools.add(school1);
        schools.add(school2);
        schools.add(school3);
        return schools;

    }

    public School schoolById(int index) {
        ArrayList<School> schools = allSchools();
        School schoolRe = new School();
        if (index < 0 || index > 3) {
            return schoolRe;
        } else {
            schoolRe = schools.get(index);
        }
        return schoolRe;
    }

    public ArrayList<School> schoolByNoNumber(int noNumber) {
        ArrayList<School> schools = allSchools();
        ArrayList<School> schoolsRe = new ArrayList<>();
        for (School school : schools) {
            if (school.getTotalStudent() > noNumber) {
                schoolsRe.add(school);
            }
        }
        return schoolsRe;
    }

}
